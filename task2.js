const user = {
    name: "Your Name",
    address: {
      personal: {
        line1: "101",
        line2: "street Line",
        city: "NY",
        state: "WX",
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
      office: {
        city: "City",
        state: "WX",
        area: {
          landmark: "landmark",
        },
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
    },
    contact: {
      phone: {
        home: "xxx",
        office: "yyy",
      },
      other: {
        fax: '234'
      },
      email: {
        home: "xxx",
        office: "yyy",
      },
    },
  };

  let outputObj = {}
  let recursive = (obj,name) => {
    for(let key in obj){
      if (typeof obj[key] == 'object'){
        recursive(obj[key],name+"_"+key)
      }
      else{
        outputObj[name+"_"+key]=obj[key]
      }
    }
    
  }


  recursive(user,'user')
  console.log(outputObj);
